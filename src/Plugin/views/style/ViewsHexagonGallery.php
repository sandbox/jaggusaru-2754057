<?php

namespace Drupal\views_hexagon_gallery\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
/**
 * Style plugin to render each item in an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "views_hexagon_gallery",
 *   title = @Translation("Views Hexogon Gallery"),
 *   help = @Translation("Displays images as Hexogon Gallery."),
 *   theme = "views_hexagon_gallery",
 *   display_types = {"normal"}
 * )
 */
class ViewsHexagonGallery extends StylePluginBase {

  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Set default options
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['type'] = array('default' => 'ul');
    $options['class'] = array('default' => '');
	$options['id'] = array('default' => 'hexGrid');
   
    return $options;
  }

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    
	$form['id'] = array(
      '#title' => $this->t('HexGon List ID'),
      '#description' => $this->t('The ID translates all photos into hexagon display.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['id'],
    );
  }

}
