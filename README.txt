Views Hexagon Gallery is a module used to display your images in a CSS3 based hexagonal grid layout with hover caption support.
Installation

    - Download and install as other modules.
    - Add a content type with image field
    - Add some contents for the gallery
    - Create a view with display format as Views Hexagon Gallery
    - Add title, body and image filed
    - Save view 